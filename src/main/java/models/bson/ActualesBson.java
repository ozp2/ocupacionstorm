package models.bson;

import models.xml.Actuales;

public class ActualesBson {

	private String lluviaultimahora;

	private String puntoderocio;

	private String velocidadviento;

	private String temperatura;

	private String humedad;

	private String intensidadlluvia;

	private String lluvia;

	private String presion;

	private String direccionviento;

	private String fecha;

	private String hora;

	private String estacion;

	private String codigoestacion;
	
    private String indiceuv;

    private String radiacionsolar;

    private String evaporacion;

	public ActualesBson(Actuales actuales) {
		super();
	}

	public ActualesBson(Actuales actuales, String fecha, String hora, String estacion, String codigoestacion) {
		super();
		this.lluviaultimahora = actuales.getLluviaultimahora();
		this.puntoderocio = actuales.getPuntoderocio();
		this.velocidadviento = actuales.getVelocidadviento();
		this.temperatura = actuales.getTemperatura();
		this.humedad = actuales.getHumedad();
		this.intensidadlluvia = actuales.getIntensidadlluvia();
		this.lluvia = actuales.getLluvia();
		this.presion = actuales.getPresion();
		this.direccionviento = actuales.getDireccionviento();
		this.indiceuv = actuales.getIndiceuv();
		this.radiacionsolar = actuales.getRadiacionsolar();
		this.evaporacion = actuales.getEvaporacion();
		this.fecha = fecha;
		this.hora = hora;
		this.estacion = estacion;
		this.codigoestacion  = codigoestacion;
	}

	public String getCodigoestacion() {
		return codigoestacion;
	}

	public void setCodigoestacion(String codigoestacion) {
		this.codigoestacion = codigoestacion;
	}

	public String getIndiceuv() {
		return indiceuv;
	}

	public void setIndiceuv(String indiceuv) {
		this.indiceuv = indiceuv;
	}

	public String getRadiacionsolar() {
		return radiacionsolar;
	}

	public void setRadiacionsolar(String radiacionsolar) {
		this.radiacionsolar = radiacionsolar;
	}

	public String getEvaporacion() {
		return evaporacion;
	}

	public void setEvaporacion(String evaporacion) {
		this.evaporacion = evaporacion;
	}

	public String getLluviaultimahora() {
		return lluviaultimahora;
	}

	public void setLluviaultimahora(String lluviaultimahora) {
		this.lluviaultimahora = lluviaultimahora;
	}

	public String getPuntoderocio() {
		return puntoderocio;
	}

	public void setPuntoderocio(String puntoderocio) {
		this.puntoderocio = puntoderocio;
	}

	public String getVelocidadviento() {
		return velocidadviento;
	}

	public void setVelocidadviento(String velocidadviento) {
		this.velocidadviento = velocidadviento;
	}

	public String getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(String temperatura) {
		this.temperatura = temperatura;
	}

	public String getHumedad() {
		return humedad;
	}

	public void setHumedad(String humedad) {
		this.humedad = humedad;
	}

	public String getIntensidadlluvia() {
		return intensidadlluvia;
	}

	public void setIntensidadlluvia(String intensidadlluvia) {
		this.intensidadlluvia = intensidadlluvia;
	}

	public String getLluvia() {
		return lluvia;
	}

	public void setLluvia(String lluvia) {
		this.lluvia = lluvia;
	}

	public String getPresion() {
		return presion;
	}

	public void setPresion(String presion) {
		this.presion = presion;
	}

	public String getDireccionviento() {
		return direccionviento;
	}

	public void setDireccionviento(String direccionviento) {
		this.direccionviento = direccionviento;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getEstacion() {
		return estacion;
	}

	public void setEstacion(String estacion) {
		this.estacion = estacion;
	}

	public String getCodigoestaccion() {
		return codigoestacion;
	}

	public void setCodigoestaccion(String codigoestacion) {
		this.codigoestacion = codigoestacion;
	}
}
