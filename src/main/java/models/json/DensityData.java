package models.json;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class DensityData {


	private String beachId;

	private List<Cell> cells = null;

	private Double occupation;

	private Integer timestamp;
	
	private String date;

	private Map<String, Object> additionalProperties = new HashMap<String, Object>();


	public String getBeachId() {
		return beachId;
	}


	public void setBeachId(String beachId) {
		this.beachId = beachId;
	}


	public List<Cell> getCells() {
		return cells;
	}


	public void setCells(List<Cell> cells) {
		this.cells = cells;
	}


	public Double getOccupation() {
		return occupation;
	}


	public void setOccupation(Double occupation) {
		this.occupation = occupation;
	}


	public Integer getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(Integer timestamp) {
		this.timestamp = timestamp;
	}


	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}


	public DensityData(Density density) {
		super();
		this.beachId = density.getBeachId();
		this.cells = density.getCells();
		this.occupation = density.getOccupation();
		this.timestamp = density.getTimestamp();
		this.date = this.getDateFromTimeStamp(density.getTimestamp());
		this.additionalProperties = density.getAdditionalProperties();
	}


	public String getDate() {
		return date;
	}


	public void setDate(String date) {
		this.date = date;
	}


	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}


	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
	
	private String getDateFromTimeStamp(Integer timestamp) {
		Timestamp ts1 = new Timestamp(TimeUnit.MILLISECONDS.convert(timestamp, TimeUnit.SECONDS));
		String date = new SimpleDateFormat("dd/MM/yyyy-hh:mm").format(new Date(ts1.getTime()));
		return date.substring(0, date.indexOf("-"));
	}

}