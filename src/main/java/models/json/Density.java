package models.json;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "beach_id", "cells", "occupation", "timestamp" })
public class Density {

	@JsonProperty("beach_id")
	private String beachId;
	@JsonProperty("cells")
	private List<Cell> cells = null;
	@JsonProperty("occupation")
	private Double occupation;
	@JsonProperty("timestamp")
	private Integer timestamp;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("beach_id")
	public String getBeachId() {
		return beachId;
	}

	@JsonProperty("beach_id")
	public void setBeachId(String beachId) {
		this.beachId = beachId;
	}

	@JsonProperty("cells")
	public List<Cell> getCells() {
		return cells;
	}

	@JsonProperty("cells")
	public void setCells(List<Cell> cells) {
		this.cells = cells;
	}

	@JsonProperty("occupation")
	public Double getOccupation() {
		return occupation;
	}

	@JsonProperty("occupation")
	public void setOccupation(Double occupation) {
		this.occupation = occupation;
	}

	@JsonProperty("timestamp")
	public Integer getTimestamp() {
		return timestamp;
	}

	@JsonProperty("timestamp")
	public void setTimestamp(Integer timestamp) {
		this.timestamp = timestamp;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}