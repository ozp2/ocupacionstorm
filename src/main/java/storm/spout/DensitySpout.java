package storm.spout;

import java.util.Map;

import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.apache.storm.utils.Utils;

import constants.ConstantsUtil;
import util.UtilMethods;

public class DensitySpout extends BaseRichSpout {
	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
	SpoutOutputCollector spoutOutputCollector;

	public void open(Map<String, Object> conf, TopologyContext context, SpoutOutputCollector collector) {
		this.spoutOutputCollector = collector;
	}

	public void nextTuple() {
		String response = "";
		try {
			response = UtilMethods.getHTML(ConstantsUtil.DENSITY_URL);
			System.out.println("Density SPOUT: " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.spoutOutputCollector.emit(new Values(response));
		Utils.sleep(300000);
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields(ConstantsUtil.OUTPUT_FIELDS));
	}
}
