package storm.bolt;

import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.bson.Document;
import org.mongodb.morphia.Morphia;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import constants.ConstantsUtil;
import converter.ConvertFromXMLToBson;
import models.xml.Proyecto_Mastral_XML_V1;

public class MastralBolt extends BaseBasicBolt{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final XmlMapper xmlMapper = new XmlMapper();
	private static final Morphia morphia = new Morphia();

	public void execute(Tuple input, BasicOutputCollector collector) {
		String response = input.getString(0);
		collector.emit(new Values(response));
		morphia.mapPackage(ConstantsUtil.MODELS_BSON);
		
		Proyecto_Mastral_XML_V1 mastral = null;

		try {
			mastral = xmlMapper.readValue(response, Proyecto_Mastral_XML_V1.class);
			insertData(mastral);
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields(ConstantsUtil.OUTPUT_FIELDS));
	}
	
	private void insertData(Proyecto_Mastral_XML_V1 mastral) {
		MongoClient mongoClient = MongoClients.create(ConstantsUtil.MONGO_DB_HOST);
		MongoDatabase database = mongoClient.getDatabase(ConstantsUtil.OCUPATION_DB);
		
		MongoCollection<Document> collectionActuales = database.getCollection(ConstantsUtil.MASTRAL_ACTUALES_COLLECTION);
		collectionActuales.insertOne(Document.parse(morphia.toDBObject(ConvertFromXMLToBson.convertActuales(mastral)).toString()));
		
		MongoCollection<Document> collectionDiarios = database.getCollection(ConstantsUtil.MASTRAL_DIARIOS_COLLECTION);
		collectionDiarios.insertOne(Document.parse(morphia.toDBObject(ConvertFromXMLToBson.convertDiarios(mastral)).toString()));
		
		MongoCollection<Document> collectionMensuales = database.getCollection(ConstantsUtil.MASTRAL_MENSUALES_COLLECTION);
		collectionMensuales.insertOne(Document.parse(morphia.toDBObject(ConvertFromXMLToBson.convertMensuales(mastral)).toString()));
		
		MongoCollection<Document> collectionAnuales = database.getCollection(ConstantsUtil.MASTRAL_ANUALES_COLLECTION);
		collectionAnuales.insertOne(Document.parse(morphia.toDBObject(ConvertFromXMLToBson.convertAnuales(mastral)).toString()));
	}
}
