package storm.bolt;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.bson.Document;
import org.mongodb.morphia.Morphia;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import constants.ConstantsUtil;
import models.json.Density;
import models.json.DensityData;

public class DensityBolt extends BaseBasicBolt {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Morphia morphia = new Morphia();
	private ObjectMapper objectMapper = new ObjectMapper();

	public void execute(Tuple input, BasicOutputCollector collector) {
		String response = input.getString(0);
		collector.emit(new Values(response));
		morphia.mapPackage(ConstantsUtil.MODELS_BSON);
		List<Density> density = null;

		try {
			density = objectMapper.readValue(response, new TypeReference<List<Density>>() {
			});
			insertData(density.get(0));
		} catch (IOException e) {
		}
	}

	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields(ConstantsUtil.OUTPUT_FIELDS));
	}

	private void insertData(Density density) {
		MongoClient mongoClient = MongoClients.create(ConstantsUtil.MONGO_DB_HOST);
		MongoDatabase database = mongoClient.getDatabase(ConstantsUtil.OCUPATION_DB);
		MongoCollection<Document> collectionDensity = database.getCollection(ConstantsUtil.DENSITY_COLLECTION);
		AggregateIterable<Document> document = collectionDensity
				.aggregate(Arrays.asList(match(and(eq(ConstantsUtil.OCUPATION_FIELD, density.getOccupation()),eq(ConstantsUtil.DENSITY_FIELD,density.getTimestamp())))));
		if (document.first() == null) {
			collectionDensity.insertOne(Document.parse(morphia.toDBObject(new DensityData(density)).toString()));
		}
	}
}
