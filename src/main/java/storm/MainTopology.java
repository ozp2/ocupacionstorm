package storm;

import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.topology.TopologyBuilder;

import constants.ConstantsUtil;
import storm.bolt.DensityBolt;
import storm.bolt.MastralBolt;
import storm.spout.DensitySpout;
import storm.spout.MastralSpout;

public class MainTopology {
	public static void main(String[] args) {
        
		TopologyBuilder builder = new TopologyBuilder();
		builder.setSpout(ConstantsUtil.MASTRAL_SPOUT, new MastralSpout());
		builder.setBolt(ConstantsUtil.MASTRAL_BOLT, new MastralBolt()).shuffleGrouping(ConstantsUtil.MASTRAL_SPOUT);
		
		builder.setSpout(ConstantsUtil.DENSITY_SPOUT, new DensitySpout());
		builder.setBolt(ConstantsUtil.DENSITY_BOLT, new DensityBolt()).shuffleGrouping(ConstantsUtil.DENSITY_SPOUT);

		Config config = new Config();
		LocalCluster cluster;
		
		try {
			cluster = new LocalCluster();
			cluster.submitTopology(ConstantsUtil.OCUPATION_TOPOLOGY, config, builder.createTopology());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
