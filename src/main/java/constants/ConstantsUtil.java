package constants;

public class ConstantsUtil {
	public static final String OCUPATION_TOPOLOGY = "OcupationTopology";
	public static final String MASTRAL_SPOUT = "MastralSpout";
	public static final String MASTRAL_BOLT = "MastralBolt";
	public static final String DENSITY_SPOUT = "DensitySpout";
	public static final String DENSITY_BOLT = "DensityBolt";
	public static final String OUTPUT_FIELDS = "fields";
	
	
	public static final String MODELS_BSON = "models.bson";
	public static final String MONGO_DB_HOST = "mongodb://localhost:27017";
	public static final String OCUPATION_DB = "ocupation";
	public static final String DENSITY_COLLECTION = "density";
	public static final String MASTRAL_ACTUALES_COLLECTION = "mastral-actuales";
	public static final String MASTRAL_DIARIOS_COLLECTION = "mastral-diarios";
	public static final String MASTRAL_MENSUALES_COLLECTION = "mastral-mensuales";
	public static final String MASTRAL_ANUALES_COLLECTION = "mastral-anuales";
	
	public static final String DENSITY_URL = "http://apicet.westeurope.cloudapp.azure.com:7000/ion_beach/density/LC1";
	public static final String MASTRAL_URL = "http://www.eltiempoenloslocos.com/loslocos";
	public static final String GET_METHOD = "GET";
	
	public static final String OCUPATION_FIELD = "ocupation";
	public static final String DENSITY_FIELD = "density";
}
