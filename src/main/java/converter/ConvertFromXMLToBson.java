package converter;

import models.bson.ActualesBson;
import models.bson.AnualesBson;
import models.bson.DiariosBson;
import models.bson.MensualesBson;
import models.xml.Proyecto_Mastral_XML_V1;

public class ConvertFromXMLToBson {
	
	public static ActualesBson convertActuales(Proyecto_Mastral_XML_V1 mastral) {
		return new ActualesBson(mastral.getActuales(), mastral.getFecha(), mastral.getHora(), mastral.getEstacion(),
				mastral.getCodigoestacion());
	}
	public static AnualesBson convertAnuales(Proyecto_Mastral_XML_V1 mastral) {
		return new AnualesBson(mastral.getAnuales(), mastral.getFecha(), mastral.getHora(), mastral.getEstacion(),
				mastral.getCodigoestacion());
	}
	public static MensualesBson convertMensuales(Proyecto_Mastral_XML_V1 mastral) {
		return new MensualesBson(mastral.getMensuales(), mastral.getFecha(), mastral.getHora(), mastral.getEstacion(),
				mastral.getCodigoestacion());
	}
	public static DiariosBson convertDiarios(Proyecto_Mastral_XML_V1 mastral) {
		return new DiariosBson(mastral.getDiarios(), mastral.getFecha(), mastral.getHora(), mastral.getEstacion(),
				mastral.getCodigoestacion());
	}
}
